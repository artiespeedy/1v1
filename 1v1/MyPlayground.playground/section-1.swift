// Playground - noun: a place where people can play

import Foundation

let date = NSDate()
let calendar = NSCalendar.currentCalendar()
let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: date)
let hour = components.hour
let minutes = components.minute
let seconds = Int((date.timeIntervalSince1970 / 100 - floor(date.timeIntervalSince1970 / 100)) * 1000)

extension String {
    mutating func removeChar(char: String) {
        self = self.stringByReplacingOccurrencesOfString(char, withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func getInfoBetweenStrings(tag: String, endPoint: String) -> String {
        var startIndex = self.rangeOfString(tag, options: .LiteralSearch, range: nil, locale: nil)!.endIndex
        var endPointSearchRange = Range<String.Index>(start: startIndex, end: self.endIndex)
        var endIndex = self.rangeOfString(endPoint, options: .LiteralSearch, range: endPointSearchRange, locale: nil)!.endIndex
        return self.substringWithRange(Range<String.Index>(start: startIndex, end: advance(endIndex, -1)))
    }
}

enum PacketType: Int {
    case Update = 0
    case Signal = 1
    case Request = 2
}

protocol Transmittable {
    func toString() -> String
    init(data: String)
}

protocol Packet: Transmittable {
    var pType: PacketType {get}
    var timeStamp: NSTimeInterval {get set}
}

enum CharState: Int {
    case None = 0
    case Dead = 1
    case Frozen = 2
    case OnFire = 3
    case Stunned = 4
    
    static func getState(num: Int) -> CharState {
        switch (num) {
        case 0:
            return None
        case 1:
            return Dead
        case 2:
            return Frozen
        case 3:
            return OnFire
        case 4:
            return Stunned
        default:
            return None
        }
    }
}

protocol GameType {
    class func getMove(moveNum: Int) -> Self
    class var nilValue: Self {get}
    var stringValue: String {get}
}

enum SwordsmanMove: Int, GameType {
    case None = 20
    case Right = 0
    case Left = 1
    case BlockUpper = 2
    case BlockMid = 3
    case BlockLower = 4
    case SlashUp = 5
    case SlashDown = 6
    
    static var nilValue: SwordsmanMove {
        get {
            return .None
        }
    }
    
    var stringValue: String {
        get {
            switch(self.rawValue) {
            case 0:
                return "Right"
            case 1:
                return "Left"
            case 2:
                return "BlockUpper"
            case 3:
                return "BlockMid"
            case 4:
                return "BlockLower"
            case 5:
                return "SlashUp"
            case 6:
                return "SlashDown"
                
            default:
                return "None"
            }
        }
    }
    
    static func getMove(moveNum: Int) -> SwordsmanMove {
        switch(moveNum) {
        case 0:
            return .Right
        case 1:
            return .Left
        case 2:
            return .BlockUpper
        case 3:
            return .BlockMid
        case 4:
            return .BlockLower
        case 5:
            return .SlashUp
        case 6:
            return .SlashDown
            
        default:
            return .None
        }
    }
}

class Turn<T: GameType>: Transmittable {
    var moves: [Int]
    
    subscript(x: Int) -> Int {
        get {
            return moves[x]
        }
        set (newValue) {
            moves[x] = newValue
        }
    }
    
    init() {
        moves = []
    }
    
    init(moves: [Int]) {
        self.moves = moves
    }
    
    //Format: <[1, 2, 3]>
    //1, 2, 3 being numbers that represent the enum
    required init(data: String) {
        
        //Clean bits
        var cleanData = data
        cleanData.removeChar(" ")
        cleanData.removeChar("<")
        cleanData.removeChar(">")
        
        //Put the string array into a Int array
        var turnMoves = cleanData.componentsSeparatedByString(",")
        
        moves = []
        
        for i in 0..<turnMoves.count {
            moves.append((turnMoves[i] as NSString).integerValue)
        }
    }
    
    //Turns Int array into a Move array
    var moveSet: [T] {
        get {
            var set : [T] = [T](count: moves.count, repeatedValue: T.nilValue)
            for i in 0..<moves.count {
                var move = T.getMove(moves[i]) as T
                println(move.stringValue)
                println(i)
                set[i] = move
            }
            
            return set
        }
    }
    
    //Turns into packet to send off
    func toString() -> String {
        var product = "["
        
        for i in moves {
            product += "\(i),"
        }
        
        product += "]"
        return product
    }
}

//Health is defined as a + or - added to the character.
class CharUpdate<T: GameType>: Transmittable {
    
    var healthChange: Int = 0
    var stateChange: CharState = .None
    var turnChange: Turn<T> = Turn<T>()
    
    /*required init(data: NSData) {
    var buffer: [Byte] = data.toArray(Byte(0))
    println(buffer)
    
    healthChange = Int(buffer[0]) - 128
    stateChange = CharState.getState(Int(buffer[1]))
    var turnNums = [Int](count: buffer.count - 2, repeatedValue: 0)
    
    for i in 2..<buffer.count {
    var num = Int(buffer[i])
    turnNums[i - 2] = num
    }
    
    turnChange = Turn<T>(moves: turnNums)
    }*/
    
    init() {
        
    }
    
    //Data format is "<healthChange:1;stateChange:2;turnChange:(<[1, 2, 3]>);>
    required init(data: String) {
        //Remove unnessary bits
        var cleanData = data
        cleanData.removeChar(" ")
        cleanData.removeChar("<")
        cleanData.removeChar(">")
        
        //Split data into the bits of
        var components = cleanData.componentsSeparatedByString(";")
        
        var values: [String] = []
        for i in components {
            var value = i.componentsSeparatedByString(":")[1]
            values.append(value)
        }
        
        //Set Health. Extra is in case health is a negative value
        var healthIsNegative = 1
        if values[0].rangeOfString("-") != nil {
            healthIsNegative = -1
            values[0].replaceRange(values[0].rangeOfString("-")!, with: "")
        }
        
        healthChange = (values[0] as NSString).integerValue * healthIsNegative
        
        //Set the state
        stateChange = CharState.getState((values[1] as NSString).integerValue)
        
        //Set the turns. Could have multiple
        turnChange = Turn<T>(data: values[2])
    }
    
    init(healthChange: Int, stateChange: CharState, turnChange: Turn<T>) {
        self.healthChange = healthChange
        self.stateChange = stateChange
        self.turnChange = turnChange
    }
    
    //Used to combine all data into one string to send off
    func toString() -> String {
        var turnString = ""
        
        for i in turnChange.moves {
            turnString += "\(i),"
        }
        
        var product = "<healthChange:\(healthChange);stateChange:\(stateChange.rawValue);turnChange:[\(turnString)]>"
        
        return product
    }
}

class UpdatePacket<T: GameType>: Packet {
    //Essentially name: charupdates
    var charUpdates: [String: CharUpdate<T>]
    
    let pType: PacketType = .Update
    var timeStamp: NSTimeInterval
    
    //Time is in milliseconds
    //Timestamp Format: hhmmsss
    //Format: (packettype)<"name":<healthChange:0;stateChange:0;turnChange:<[1, 2, 3]>>>\n ...
    required init(data: String) {
        var mData = data
        
        var range = mData.rangeOfString(")", options: .LiteralSearch, range: nil, locale: nil)?
        
        var endOfTag = advance(range!.endIndex, 1)
        
        var tagRange = Range<String.Index>(start: mData.startIndex, end: endOfTag)
        var tag = mData.substringWithRange(tagRange)
        //Now get the info from the tag
        //Info should look like <(tag:update;timestamp:TIME;):
        
        var stringTimeStamp = tag.getInfoBetweenStrings("timestamp:", endPoint: ";")
        timeStamp = NSTimeInterval(stringTimeStamp.toInt()!)
        
        //Clean up
        mData.replaceRange(tagRange, with: "")
        
        //Get each part of the array
        var instructions = mData.componentsSeparatedByString("\n")
        
        self.charUpdates = [:]
        
        for i in instructions {
            //Clean up the <>
            var update = i
            var first = update.rangeOfString("<", options: NSStringCompareOptions.LiteralSearch, range: nil, locale: nil)!
            update.replaceRange(first, with: "")
            var second = update.rangeOfString("<", options: NSStringCompareOptions.LiteralSearch, range: nil, locale: nil)! as Range<String.Index>
            
            //Get the name
            var nameRange = NSRange(location: 0, length: distance(update.startIndex, second.endIndex) - 2)
            
            var name = (update as NSString).substringWithRange(nameRange)
            
            //Remove the whole "<name:"
            var cleanUpRange = Range<String.Index>(start: update.startIndex, end: advance(update.startIndex, (distance(update.startIndex, second.endIndex) - 1)))
            update.replaceRange(cleanUpRange, with: "")
            //Remove last >
            update.replaceRange(Range<String.Index>(start: advance(update.endIndex, -1), end: update.endIndex), with: "")
            
            //Send the rest to CharUpdate
            
            var cUpdate = CharUpdate<T>(data: update)
            
            charUpdates[name] = cUpdate
        }
    }
    
    init(updates: [String: CharUpdate<T>]) {
        timeStamp = NSTimeInterval(hour * 100000 + minutes * 1000 + seconds)
        charUpdates = updates
    }
    
    //Format: <(tag:update;timestamp:TIME;): charUpdateFormat\n...>
    func toString() -> String {
        //Add the string together
        
        var product = "<(ptype:update;timestamp:\(Int(timeStamp));):"
        
        for i in charUpdates.keys.array {
            //Turn part of the array into
            var iProduct = "<"
            iProduct += i
            iProduct += ":"
            iProduct += charUpdates[i]!.toString()
            iProduct += ">"
            product += iProduct
            product += "\n"
        }
        
        //Remove last \n
        
        var r = Range<String.Index>(start: advance(product.endIndex, -2), end: product.endIndex)
        
        product.replaceRange(r, with: "")
        
        product += ">"
        
        return product
    }
}

var t1 = Turn<SwordsmanMove>(moves: [0, 1, 3, 5])
var t2 = Turn<SwordsmanMove>(moves: [0, 2, 3, 4])
var t3 = Turn<SwordsmanMove>(moves: [1, 1, 2, 5])
var t4 = Turn<SwordsmanMove>(moves: [4, 2, 3, 4])

var c1 = CharUpdate<SwordsmanMove>(healthChange: -1, stateChange: CharState.None, turnChange: t1)
var c2 = CharUpdate<SwordsmanMove>(healthChange: 30, stateChange: CharState.OnFire, turnChange: t2)

var updates = [
    "Alex": c1,
    "Eric": c2
]

var p1 = UpdatePacket<SwordsmanMove>(updates: updates)

var packet = p1.toString()

var p2 = UpdatePacket<SwordsmanMove>(data: packet)






