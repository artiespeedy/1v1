//
//  Move.swift
//  1v1
//
//  Created by Alex Mai on 1/7/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

protocol GameType {
    class func getMove(moveNum: Int) -> Self
    class var nilValue: Self {get}
    var stringValue: String {get}
}