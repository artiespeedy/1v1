//
//  Sword.swift
//  1v1
//
//  Created by Alexander Mai on 1/25/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation
import SpriteKit

class Sword: SKSpriteNode {
    var yRotation: Float = 0 {
        didSet {
            let index = yRotation / 360 * Float(rotSprites.count)
			 
            self.texture = rotSprites[Int(index)]
        }
    }
    
    var rotSprites: [Int: SKTexture] = [:]
    
    override init() {
        super.init(texture: SKTexture(data: NSData(bytes: [Byte](), length: 0), size: CGSize(width: 0, height: 0)), color: UIColor.blackColor(), size: CGSize(width: 0, height: 0))
        anchorPoint = CGPoint(x: 0.5, y: 0)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

	
}
