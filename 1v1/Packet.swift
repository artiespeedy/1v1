//
//  Packet.swift
//  1v1
//
//  Created by Alex Mai on 1/13/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

let date = NSDate()
let calendar = NSCalendar.currentCalendar()
let components = calendar.components(.CalendarUnitHour | .CalendarUnitMinute, fromDate: date)
let hour = components.hour
let minutes = components.minute
let seconds = Int((date.timeIntervalSince1970 / 100 - floor(date.timeIntervalSince1970 / 100)) * 1000)

func getCurrentTimeStamp() -> NSTimeInterval {
    return NSTimeInterval(hour * 100000 + minutes * 1000 + seconds)
}

enum PacketType: Int {
    case Update = 0
    case Signal = 1
    case Request = 2
    case Verification = 3
    case Turn = 4
}

protocol Transmittable {
    func toString() -> String
    init(data: String)
}

protocol Packet: Transmittable {
    var pType: PacketType {get}
    var timeStamp: NSTimeInterval {get set}
}