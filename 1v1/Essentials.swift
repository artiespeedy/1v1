//
//  Essentials.swift
//  1v1
//
//  Created by Alexander Mai on 1/17/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

extension String {
    mutating func removeChar(char: String) {
        self = self.stringByReplacingOccurrencesOfString(char, withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
    
    func getInfoBetweenStrings(tag: String, endPoint: String) -> String {
        var startIndex = self.rangeOfString(tag, options: .LiteralSearch, range: nil, locale: nil)!.endIndex
        var endPointSearchRange = Range<String.Index>(start: startIndex, end: self.endIndex)
        var endIndex = self.rangeOfString(endPoint, options: .LiteralSearch, range: endPointSearchRange, locale: nil)!.endIndex
        return self.substringWithRange(Range<String.Index>(start: startIndex, end: advance(endIndex, -1)))
    }
}

extension NSData {
    func toArray<T>(placeholder: T) -> [T] {
        let size = sizeof(T)
        let count = self.length / size
        var array = [T](count: count, repeatedValue: placeholder)
        
        self.getBytes(&array, length: size * count)
        
        return array
    }
}

extension Int {
    var isNegative : Bool {
        get {
            if (self==0) {
                return false
            } else {
                return -1==(self / abs(self))
            }
        }
    }
    
    func isolate(index : Int) -> Int? {
        let dCount = digitCount()
        let distanceToDigit = Int(pow(10, Double((dCount - index))))
        if distanceToDigit < 1 {
            return nil
        } else {
            let isolationDivider = distanceToDigit * 10
            return self / distanceToDigit - self / isolationDivider * 10
        }
    }
    
    func digitCount() -> Int {
        var count : Int
        (self == 0) ? (count = 1) : (count = Int(log10(Double(self))) + 1)
        return count
    }
}

