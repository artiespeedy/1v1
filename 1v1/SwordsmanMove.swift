//
//  SwordsmanMove.swift
//  1v1
//
//  Created by Alexander Mai on 1/20/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

enum SwordsmanMove: Int, GameType {
    case None = 20
    case Right = 0
    case Left = 1
    case BlockUpper = 2
    case BlockMid = 3
    case BlockLower = 4
    case SlashUp = 5
    case SlashDown = 6
    
    static var nilValue: SwordsmanMove {
        get {
            return .None
        }
    }
    
    var stringValue: String {
        get {
            switch(self.rawValue) {
            case 0:
                return "Right"
            case 1:
                return "Left"
            case 2:
                return "BlockUpper"
            case 3:
                return "BlockMid"
            case 4:
                return "BlockLower"
            case 5:
                return "SlashUp"
            case 6:
                return "SlashDown"
                
            default:
                return "None"
            }
        }
    }
    
    static func getMove(moveNum: Int) -> SwordsmanMove {
        switch(moveNum) {
        case 0:
            return .Right
        case 1:
            return .Left
        case 2:
            return .BlockUpper
        case 3:
            return .BlockMid
        case 4:
            return .BlockLower
        case 5:
            return .SlashUp
        case 6:
            return .SlashDown
            
        default:
            return .None
        }
    }
}