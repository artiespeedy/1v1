//
//  Turn.swift
//  1v1
//
//  Created by Alex Mai on 12/14/14.
//  Copyright (c) 2014 Alex Mai. All rights reserved.
//

import Foundation

class Turn<T: GameType>: Packet {
    var moves: [Int]
    
    let pType = PacketType.Turn
    var timeStamp: NSTimeInterval
    
    subscript(x: Int) -> Int {
        get {
            return moves[x]
        }
        set (newValue) {
            moves[x] = newValue
        }
    }
    
    init() {
        moves = []
        timeStamp = getCurrentTimeStamp()
    }
    
    init(moves: [Int]) {
        self.moves = moves
        timeStamp = getCurrentTimeStamp()
    }
    
    //Format: <[1, 2, 3]>
    //1, 2, 3 being numbers that represent the enum
    required init(data: String) {
        
        timeStamp = NSTimeInterval(data.getInfoBetweenStrings("timestamp:", endPoint: ";").toInt()!)
        
        var cleanData = data.getInfoBetweenStrings("[", endPoint: "]")
        
        //Put the string array into a Int array
        var turnMoves = cleanData.componentsSeparatedByString(",")
        
        moves = []
        
        for i in 0..<turnMoves.count {
            moves.append((turnMoves[i] as NSString).integerValue)
        }
    }
    
    //Turns Int array into a Move array
    var moveSet: [T] {
        get {
            var set : [T] = [T](count: moves.count, repeatedValue: T.nilValue)
            for i in 0..<moves.count {
                var move = T.getMove(moves[i]) as T
                println(move.stringValue)
                println(i)
                set[i] = move
            }
            
            return set
        }
    }
    
    //Turns into packet to send off
    func toString() -> String {
        var product = "<(tag:turn;timestamp:\(timeStamp)):["
        
        for i in moves {
            product += "\(i),"
        }
        
        product += "]>"
        return product
    }
}