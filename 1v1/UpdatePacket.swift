//
//  UpdatePacket.swift
//  1v1
//
//  Created by Alex Mai on 1/13/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

typealias PlayerIndex = Int

class UpdatePacket<T: GameType>: Packet {
    //Essentially name: charupdates
    var charUpdates: [String: CharUpdate<T>]
    
    let pType: PacketType = .Update
    var timeStamp: NSTimeInterval
    
    //Time is in milliseconds
    //Timestamp Format: hhmmsss
    //Format: (packettype)<"name":<healthChange:0;stateChange:0;turnChange:<[1, 2, 3]>>>\n ...
    required init(data: String) {
        var mData = data
        
        var range = mData.rangeOfString(")", options: .LiteralSearch, range: nil, locale: nil)?
        
        var endOfTag = advance(range!.endIndex, 1)
        
        var tagRange = Range<String.Index>(start: mData.startIndex, end: endOfTag)
        var tag = mData.substringWithRange(tagRange)
        //Now get the info from the tag
        //Info should look like <(tag:update;timestamp:TIME;):
        
        var stringTimeStamp = tag.getInfoBetweenStrings("timestamp:", endPoint: ";")
        timeStamp = NSTimeInterval(stringTimeStamp.toInt()!)
        
        //Clean up
        mData.replaceRange(tagRange, with: "")
        
        //Get each part of the array
        var instructions = mData.componentsSeparatedByString("\n")
        
        self.charUpdates = [:]
        
        for i in instructions {
            //Clean up the <>
            var update = i
            var first = update.rangeOfString("<", options: NSStringCompareOptions.LiteralSearch, range: nil, locale: nil)!
            update.replaceRange(first, with: "")
            var second = update.rangeOfString("<", options: NSStringCompareOptions.LiteralSearch, range: nil, locale: nil)! as Range<String.Index>
            
            //Get the name
            var nameRange = NSRange(location: 0, length: distance(update.startIndex, second.endIndex) - 2)
            
            var name = (update as NSString).substringWithRange(nameRange)
            
            //Remove the whole "<name:"
            var cleanUpRange = Range<String.Index>(start: update.startIndex, end: advance(update.startIndex, (distance(update.startIndex, second.endIndex) - 1)))
            update.replaceRange(cleanUpRange, with: "")
            //Remove last >
            update.replaceRange(Range<String.Index>(start: advance(update.endIndex, -1), end: update.endIndex), with: "")
            
            //Send the rest to CharUpdate
            
            var cUpdate = CharUpdate<T>(data: update)
            
            charUpdates[name] = cUpdate
        }
    }
    
    init(updates: [String: CharUpdate<T>]) {
        timeStamp = getCurrentTimeStamp()
        charUpdates = updates
    }
    
    //Format: <(tag:update;timestamp:TIME;): charUpdateFormat\n...>
    func toString() -> String {
        //Add the string together
        
        var product = "<(ptype:update;timestamp:\(Int(timeStamp));):"
        
        for i in charUpdates.keys.array {
            //Turn part of the array into
            var iProduct = "<"
            iProduct += i
            iProduct += ":"
            iProduct += charUpdates[i]!.toString()
            iProduct += ">"
            product += iProduct
            product += "\n"
        }
        
        //Remove last \n
        
        var r = Range<String.Index>(start: advance(product.endIndex, -2), end: product.endIndex)
        
        product.replaceRange(r, with: "")
        
        product += ">"
        
        return product
    }
}
