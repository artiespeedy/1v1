//
//  Server.swift
//  1v1
//
//  Created by Alex Mai on 12/14/14.
//  Copyright (c) 2014 Alex Mai. All rights reserved.
//

import Foundation
import MultipeerConnectivity

class Server<T: GameType>: NSObject, MCSessionDelegate {
    var turnNumber: Int = 0
    var session: MCSession
    var updateQueue: [Instruction] = []
    var movesPerTurn = 1
    
    init(host: MCPeerID, peer: MCPeerID, session: MCSession) {
        self.session = session
        
    }
    
    func isTurnLegal(turn: Turn<T>) {
        
    }
    
    func update(turn: Turn<T>) {
        
    }
    
    func session(session: MCSession!, didReceiveData data: NSData!, fromPeer peerID: MCPeerID!) {
        //Turn data into turn
        var turn = Turn<T>()
        
        update(turn)
    }
    
    func session(session: MCSession!, peer peerID: MCPeerID!, didChangeState state: MCSessionState) {
        
    }
    
    func session(session: MCSession!, didStartReceivingResourceWithName resourceName: String!, fromPeer peerID: MCPeerID!, withProgress progress: NSProgress!) {
        
    }
    
    func session(session: MCSession!, didFinishReceivingResourceWithName resourceName: String!, fromPeer peerID: MCPeerID!, atURL localURL: NSURL!, withError error: NSError!) {
        
    }
    
    func session(session: MCSession!, didReceiveStream stream: NSInputStream!, withName streamName: String!, fromPeer peerID: MCPeerID!) {
        
    }
}

class Instruction {
    var target: Target
    var action: InstructionAction
    
    init(target: Target, action: InstructionAction) {
        self.target = target
        self.action = action
    }
}

protocol Target {
    var tag: String {get}
}

func == (rhs: Target, lhs: Target) -> Bool{
    return (rhs.tag == lhs.tag)
}

protocol InstructionAction {
    
}