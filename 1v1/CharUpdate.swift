//
//  CharUpdate.swift
//  1v1
//
//  Created by Alex Mai on 1/13/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

enum CharState: Int {
    case None = 0
    case Dead = 1
    case Frozen = 2
    case OnFire = 3
    case Stunned = 4
    
    static func getState(num: Int) -> CharState {
        switch (num) {
        case 0:
            return None
        case 1:
            return Dead
        case 2:
            return Frozen
        case 3:
            return OnFire
        case 4:
            return Stunned
        default:
            return None
        }
    }
}

//Health is defined as a + or - added to the character.
class CharUpdate<T: GameType>: Transmittable {
    
    var healthChange: Int = 0
    var stateChange: CharState = .None
    var turnChange: Turn<T> = Turn<T>()
    
    /*required init(data: NSData) {
        var buffer: [Byte] = data.toArray(Byte(0))
        println(buffer)
        
        healthChange = Int(buffer[0]) - 128
        stateChange = CharState.getState(Int(buffer[1]))
        var turnNums = [Int](count: buffer.count - 2, repeatedValue: 0)
        
        for i in 2..<buffer.count {
            var num = Int(buffer[i])
            turnNums[i - 2] = num
        }
        
        turnChange = Turn<T>(moves: turnNums)
    }*/
    
    init() {
        
    }
    
    //Data format is "<healthChange:1;stateChange:2;turnChange:(<[1, 2, 3]>);>
    required init(data: String) {
        //Remove unnessary bits
        var cleanData = data
        cleanData.removeChar(" ")
        cleanData.removeChar("<")
        cleanData.removeChar(">")
        
        //Split data into the bits of
        var components = cleanData.componentsSeparatedByString(";")
        
        var values: [String] = []
        for i in components {
            var value = i.componentsSeparatedByString(":")[1]
            values.append(value)
        }
        
        //Set Health. Extra is in case health is a negative value
        var healthIsNegative = 1
        if values[0].rangeOfString("-") != nil {
            healthIsNegative = -1
            values[0].replaceRange(values[0].rangeOfString("-")!, with: "")
        }
        
        healthChange = (values[0] as NSString).integerValue * healthIsNegative
        
        //Set the state
        stateChange = CharState.getState((values[1] as NSString).integerValue)
        
        //Set the turns. Could have multiple
        turnChange = Turn<T>(data: values[2])
    }
    
    init(healthChange: Int, stateChange: CharState, turnChange: Turn<T>) {
        self.healthChange = healthChange
        self.stateChange = stateChange
        self.turnChange = turnChange
    }
    
    //Used to combine all data into one string to send off
    func toString() -> String {
        var turnString = ""
        
        for i in turnChange.moves {
            turnString += "\(i),"
        }
        
        var product = "<healthChange:\(healthChange);stateChange:\(stateChange.rawValue);turnChange:[\(turnString)]>"
        
        return product
    }
}