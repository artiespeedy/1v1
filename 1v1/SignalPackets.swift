//
//  SignalPackets.swift
//  1v1
//
//  Created by Alexander Mai on 1/18/15.
//  Copyright (c) 2015 Alex Mai. All rights reserved.
//

import Foundation

enum EndGameCauses: Int {
    case GameOver = 0
    case Disconnect = 1
    case Error = 2
    
    static func getCause(name: String) -> EndGameCauses {
        switch(name) {
        case "gameover":
            return .GameOver
        case "disconnect":
            return .Disconnect
        case "error":
            return .Error
        default:
            return .GameOver
        }
    }
}

class EndGamePacket: Packet {
    var pType = PacketType.Signal
    var timeStamp: NSTimeInterval
    var cause: EndGameCauses
    
    required init(data: String) {
        timeStamp = NSTimeInterval(hour * 100000 + minutes * 1000 + seconds)
        //Lazy, doesn't remove excess
        cause = EndGameCauses.getCause(data)
    }
    
    init(cause: EndGameCauses) {
        self.cause = cause
        timeStamp = getCurrentTimeStamp()
    }
    
    func toString() -> String {
        return "<(tag:signal;timestamp:\(timeStamp)):<message:endgame;cause:\(cause);>>"
    }
}

class EndRoundPacket: Packet {
    var pType = PacketType.Signal
    var timeStamp: NSTimeInterval
    var winner: String
    
    required init(data: String) {
        timeStamp = NSTimeInterval(hour * 100000 + minutes * 1000 + seconds)
        winner = data
    }
    
    init(winner: String) {
        self.winner = winner
        timeStamp = getCurrentTimeStamp()
    }
    
    func toString() -> String {
        return "<(tag:signal;timestamp:\(timeStamp)):<message:endround;winner:\(winner);>>"
    }
}