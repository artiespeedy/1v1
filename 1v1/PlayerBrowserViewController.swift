//
//  PlayerBrowserViewController.swift
//  1v1
//
//  Created by Alex Mai on 12/22/14.
//  Copyright (c) 2014 Alex Mai. All rights reserved.
//

import Foundation
import UIKit
import MultipeerConnectivity

class PlayerBrowserViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MCNearbyServiceBrowserDelegate, MCNearbyServiceAdvertiserDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var duelmenu: UIView!
    @IBOutlet weak var swipeRecog: UISwipeGestureRecognizer!
    
    var availablePlayers: [MCPeerID: Int] = [:]
    var selfID = MCPeerID(displayName: UIDevice.currentDevice().name)
    var browser: MCNearbyServiceBrowser?
    var advert: MCNearbyServiceAdvertiser?
    var session: MCSession?
    var lastSelection: (MCPeerID, NSIndexPath)?
    var lastInvite: MCPeerID?
    
    override func viewDidLoad() {
        tableview.delegate = self
        tableview.dataSource = self
        browser = MCNearbyServiceBrowser(peer: selfID, serviceType: "1v1Challenge")
        browser!.delegate = self
        advert = MCNearbyServiceAdvertiser(peer: selfID, discoveryInfo: nil, serviceType: "1v1Challenge")
        advert!.delegate = self
        session = MCSession(peer: selfID)
        swipeRecog.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        browser!.startBrowsingForPeers()
        advert!.startAdvertisingPeer()
    }
    
    override func viewDidDisappear(animated: Bool) {
        browser!.stopBrowsingForPeers()
        advert!.startAdvertisingPeer()
    }
    
    @IBAction func hideTableView() {
        var newFrame = duelmenu.frame
        newFrame.origin.x -= newFrame.width
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseOut)
        UIView.setAnimationDelegate(self)
        UIView.setAnimationDelay(0)
        UIView.setAnimationDuration(0.2)
        duelmenu.frame = newFrame
        UIView.commitAnimations()
    }
    
    @IBAction func showTableView() {
        var newFrame = duelmenu.frame
        newFrame.origin.x += newFrame.width
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseOut)
        UIView.setAnimationDelegate(self)
        UIView.setAnimationDelay(0)
        UIView.setAnimationDuration(0.2)
        duelmenu.frame = newFrame
        UIView.commitAnimations()
    }
    
    @IBAction func swipeHideTableView() {
        var newFrame = duelmenu.frame
        newFrame.origin.x -= newFrame.width
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(UIViewAnimationCurve.EaseOut)
        UIView.setAnimationDelegate(self)
        UIView.setAnimationDelay(0)
        UIView.setAnimationDuration(0.2)
        duelmenu.frame = newFrame
        UIView.commitAnimations()
    }
    
    
    func startServer(client : MCPeerID) {
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("LocalPlayer") as UITableViewCell?
        let index = indexPath.item
        let cellDim = tableview.rectForRowAtIndexPath(indexPath)
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "LocalPlayer")
        }
        
        if availablePlayers.count > 0 {
            var label = UILabel(frame: CGRect(x: 20, y: 0, width: cellDim.width, height: cellDim.height))
            label.text = availablePlayers.keys.array[index].displayName
            cell!.addSubview(label)
        } else {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Waiting")
            var label = UILabel(frame: CGRect(x: 20, y: 0, width: cellDim.width, height: cellDim.height))
            label.text = "Searching for players..."
            cell!.addSubview(label)
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var cell = tableView.cellForRowAtIndexPath(indexPath)
        let cellDim = tableview.rectForRowAtIndexPath(indexPath)
        
        var loadingIcon = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: cellDim.height, height: cellDim.height))
        loadingIcon.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        loadingIcon.startAnimating()
        cell!.accessoryView = loadingIcon
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        lastSelection = (availablePlayers.keys.array[indexPath.item], indexPath)
        
        browser!.invitePeer(availablePlayers.keys.array[indexPath.item], toSession: session, withContext: nil, timeout: 50)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (availablePlayers.count > 0) {return availablePlayers.count} else {return 1}
    }
    
    func browser(browser: MCNearbyServiceBrowser!, didNotStartBrowsingForPeers error: NSError!) {
        
    }
    
    func browser(browser: MCNearbyServiceBrowser!, foundPeer peerID: MCPeerID!, withDiscoveryInfo info: [NSObject : AnyObject]!) {
        availablePlayers[peerID] = availablePlayers.count
        tableview.reloadData()
    }
    
    func browser(browser: MCNearbyServiceBrowser!, lostPeer peerID: MCPeerID!) {
        availablePlayers.removeValueForKey(peerID)
        tableview.reloadData()
    }
    
    func advertiser(advertiser: MCNearbyServiceAdvertiser!, didReceiveInvitationFromPeer peerID: MCPeerID!, withContext context: NSData!, invitationHandler: ((Bool, MCSession!) -> Void)!) {
        
        lastInvite = peerID
        
        if context != nil {
            //Remove loading
            tableview.cellForRowAtIndexPath(lastSelection!.1)?.accessoryView = nil
        } else {
            var alert = UIAlertView(title: "Challenge", message: "\(peerID.displayName) has challenged you to a 1v1!", delegate: self, cancelButtonTitle: "Accept", otherButtonTitles: "Decline")
            alert.show()
        }
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if buttonIndex == 0 {
            startServer(lastInvite!)
        } else {
            let info = NSData()
            browser!.invitePeer(lastInvite!, toSession: session, withContext: info, timeout: 50)
        }
    }
}